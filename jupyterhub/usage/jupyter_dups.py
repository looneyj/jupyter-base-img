import subprocess


def get_username(l):
    strs = l.split('jupyter-')
    if len(strs) < 3:
        return None
    return strs[2]

def get_compute_host(l):
    strs = l.split('/')
    return strs[0]

def main():
    cmd = "docker -H :4000 ps -a | awk '{print $NF}'"
    p = subprocess.check_output([cmd], shell=True)
#    print(p)
#    print ("******************")

    lines = iter(p.splitlines())
#    lines = p.split('/')
#    print(lines)
    tot = 0
    containers = {}
    for l in lines:
#        print(l)
        name = get_username(l)
        host = get_compute_host(l)
        print(l, name, host)
        if not name or name == 'None':
            continue
        if name in containers:
             ct = containers[name]['ct'] + 1
             try:
                 hosts = containers[name]['hosts'].extend([host])
                 containers[name] = {'ct': ct, 'hosts': hosts}
             except Exception:
                 print("ERROR -- name: {}, containers[name]: {}".format(name, containers[name]))
        else:
             containers[name] = {'ct': 1, 'hosts': [host]}
        tot = tot + 1
#        print(l)
#        ct = 0
#        for t in lines:
#            if not t:
#                continue
#            if l in t:
#                ct = ct + 1
#                if ct > 1:
#                    print("possible dup: {}".format(l))
    print("Total number of containers: {}".format(tot))
    print("******************")
    for k,v in containers.items():
        if v['ct'] > 1:
#            print('{}:{}   hosts: {}'.format(k, v['ct'], v['hosts']))
            print(k,v)
if __name__ == '__main__':
    main()
